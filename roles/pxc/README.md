Role Name
=========

A brief description of the role goes here.

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

TODO

Dependencies
------------

None

Example Playbook
---
    - hosts: pxc57
      become: true
      tasks:
        - name: Install PXC 5.7
          ansible.builtin.include_role:
            name: alexchadwick.mysql.pxc
          vars:
            bootstrap_hostname: pxc-0
            pxc_host_addresses: >-
              {% set comma = joiner(",") %}
              {%- for host in groups['pxc-57'] -%}
                {{ comma() }}{{ hostvars[host]['ansible_eth0']['ipv4']['address'] }}
              {%- endfor -%}
            mysql_custom_config:
              wsrep_sst_auth: "sst:sst" # wsrep_sst_auth only required for pxc-57

    - hosts: pxc80
      become: true
      tasks:
        - name: Install PXC 8.0
          ansible.builtin.include_role:
            name: alexchadwick.mysql.pxc
          vars:
            bootstrap_hostname: pxc-0
            pxc_host_addresses: >-
              {% set comma = joiner(",") %}
              {%- for host in groups['pxc-80'] -%}
                {{ comma() }}{{ hostvars[host]['ansible_eth0']['ipv4']['address'] }}
              {%- endfor -%}

License
-------

MIT
